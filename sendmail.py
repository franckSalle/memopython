import sys
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate

def envoyerMail(to, cc, objet, message):
    msg = MIMEMultipart()
    msg['From'] = 'x@x.com'
    msg['To'] = ",".join(to)
    msg['Cc'] = ",".join(cc)
    dest = to + cc
    msg['Bcc'] = ''    
    msg['Subject'] = objet
    msg['Date'] = formatdate(localtime=True)
    msg['Charset'] = 'ISO-8859-15'
    msg['Content-type'] = 'text/html' 
    msg.attach(MIMEText(message, 'html'))
    mailserver = smtplib.SMTP('smtp.site.fr', 25)
    mailserver.helo()
    #mailserver.starttls()
    #mailserver.ehlo()
    #mailserver.login('XXX@gmail.com', 'PASSWORD')
    mailserver.sendmail('x@x.com', dest, msg.as_string())
    mailserver.quit()

if __name__ == '__main__': 

    to = ['y@y.com'] # adresses des destinataires
    cc = ['z@z.com'] #adresses en copie
    envoyerMail(to, cc, '[TEST]', 'Message de test envoyé depuis le serveur smtp.')