# Test with 
# [GET] curl -i http://localhost:5000/webservice
# [POST] curl -i -H "Content-Type: application/json" -X POST -d '{'id': 1, 'champ1': u'test1', 'champ2': u'test2'}' http://localhost:5000/webservice

from flask import Flask, jsonify, request

app = Flask(__name__)

data = {
    'id': 1,
    'champ1': u'test1',
    'champ2': u'test2'
    }

@app.route('/webservice', methods=['GET'])
def get():
    return jsonify(data)

@app.route('/webservice', methods=['POST'])
def create():
    if not request.json:
        return 400
    recept = request.json
    print(recept)
    return jsonify(recept)


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)